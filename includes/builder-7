#define _RHEL_IMAGE rhel
#define _USE_YUM_PKG_MANAGER 1
#define _AWSCLI_PIP_BACKPORTED 1

/* DO NOT use Dockerfile/Containerfile commands before this line */
#include "setup-from-rhel"

#include "builder-all"

/* Install the required packages for CI work. */
RUN yum -y install \
      python \
      python-devel \
      python-docutils \
      yum-utils

RUN yum -y install \
      binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu \
      binutils-powerpc64-linux-gnu gcc-powerpc64-linux-gnu \
      binutils-powerpc64le-linux-gnu \
      binutils-s390x-linux-gnu gcc-s390x-linux-gnu

/* There's only one cross compiler binary for both ppc64 and ppc64le. Kernel
 * can handle this special case but we still want correct compiler data to submit.
 * We can't symlink the two because gcc is special and overrides the target name
 * with the symlink name, likely due to how the cross compilers are actually
 * implemented as part of gcc. This is the case for all gcc packages and is not
 * RHEL7 specific:
 *
 * $ which gcc
 * /usr/bin/gcc
 * $ ln -s /usr/bin/gcc mytestfile
 * $ ./mytestfile --version
 * mytestfile (GCC) 10.2.1 20201016 (Red Hat 10.2.1-6)          <----- mytestfile
 * Copyright (C) 2020 Free Software Foundation, Inc.
 * This is free software; see the source for copying conditions.  There is NO
 * warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Because of this peculiarity, we create a shell script that invokes the correct
 * compiler instead.
 */
COPY files/powerpc64le-linux-gnu-gcc /usr/bin

RUN yum-builddep -y kernel

#include "cleanup"
